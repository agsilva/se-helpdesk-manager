<?php 
	session_start();
	
	try {
	
		$servername = "localhost";
		$username = "root";
		$password = "nhe";
		$dbname = "ES";
	
		$bd = mysqli_connect($servername, $username, $password, $dbname);
		if ($bd->connect_error) {
			$bd = false;
		}
	
	} catch (Exception $e) {
		$bd = false;
	}


	if($bd !== false)
	{
		$switchid= $_POST["radio"];
		
		$sql = "SELECT * FROM switches WHERE id ='$switchid'";
		$result = $bd->query($sql);

		$row = $result->fetch_assoc();
		
		
		
		$sql = "SELECT * FROM commands WHERE switchid ='$switchid'";
		$result = $bd->query($sql);
		
		while($nhe = $result->fetch_assoc()){
			$commands.=$nhe['command'];
			$commands.="\n";
		}
	
	
	}
	else{
		echo "NAO LIGOU À BD";
	}
	
	

?>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HelpDESK | Configurações</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
	<!-- header logo: style can be found in header.less -->
	<header class="header">
		<a href="home.php" class="logo"> <!-- Add the class icon to your logo image or logo icon to add the margining -->
			HelpDESK
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
			<!-- Sidebar toggle button-->

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<!-- Messages: style can be found in dropdown.less-->

					<!-- Notifications: style can be found in dropdown.less -->

					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <i
							class="glyphicon glyphicon-user"></i> <span>Utilizador <i
								class="caret"></i></span>
					</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header bg-light-blue"><img
								src="img/avatar5.png" class="img-circle" alt="User Image" />
								<p>Set Team - Web Developer</p></li>
							<!-- Menu Body -->

							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="[ES]Perfil.html" class="btn btn-default btn-flat">Perfil</a>
								</div>
								<div class="pull-right">
									<a href="#" class="btn btn-default btn-flat">Sair</a>
								</div>
							</li>
						</ul></li>
				</ul>
			</div>
		</nav>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="img/avatar5.png" class="img-circle" alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>Bem-Vindo</p>
					</div>
				</div>

				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li><a href="[ES]Adicionar.html"> <i class="fa fa-plus"></i>
							<span>Adicionar</span>
					</a></li>
					<li><a href="#"> <i class="fa fa-edit"></i>
							<span>Configurar</span>
					</a></li>
					<li><a href="#"> <i class="fa fa-power-off"></i> <span>Desactivar</span>

					</a></li>

					<li><a href="[ES]Historico.html"> <i class="fa fa-archive"></i>
							<span>Histórico</span>
					</a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Adicionar</h1>


			</section>

			<!-- Main content -->
			<section class="content">
				
				<div class="row">
                        <div class="col-md-6">
                            <div class="box box">
                                <div class="box-header">
                                    <i class="fa fa-cog"></i>
                                    <h3 class="box-title">Opções gerais</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="callout">
                                       <h4><label>Nome: </label> <?php echo $row['name'];?><br>
                                        <br/><label>Marca: </label> <?php echo $row['brand'];?><br>
                                        <br/><label>Modelo: </label> <?php echo $row['model'];?><br></h4>                     
                                    </div>
                                    <hr>
                                    
                                     <div class="box-header">
	                                    <i class="fa fa-signal"></i>
	                                    <h3 class="box-title">Opções de redes</h3>
                               	 	</div><!-- /.box-header -->
                                    
                                    <div class="box-body">
                                    <div class="callout"><h4>
                                    	<label>IP: </label> <?php echo $row['ip'];?><br>
                                        <br/><label>Tipo de ligação: </label> <?php echo $row['type'];?><br>
                                         <br/><label>User: </label> <?php echo $row['user'];?><br>
                                         <br/><label>Password: </label> <?php echo $row['pass'];?>
                                    </h4> 
                                    </div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->
                        
                        
                        
                        <div class="col-md-6">
                            <div class="box box">
                                <div class="box-header">
                                    <i class="fa fa-edit"></i>
                                    <h3 class="box-title">Edição das configurações</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                
                                
                                 <form id="editForm" name="editForm" method="post" action="editSwitch.php">
                                    <div class="callout">
                                    	
                                       <b>Nome</b><small>-Insira o novo username</small><input name="name" type="text"
											class="form-control" value="<?php echo $row['name'];?>" placeholder="Novo nome do Switch" /> <br/>
										
										<b>IP</b><small>-Insira o novo IP Address</small><input name="ip" type="text"  value="<?php echo $row['ip'];?>"
										class="form-control" placeholder="Novo IP do Switch" /><br/>
										
										<b>Tipo de ligação (ssh ou telnet)</b><small>-Tipo de ligação: ssh ou telnet?</small><input name="type" type="text"
										class="form-control"  value="<?php echo $row['type'];?>" placeholder="Tipo de ligação" /><br/>
										
										<b>User</b><small>-Novo username de acesso ao Switch</small><input name="user" type="text"
										class="form-control"  value="<?php echo $row['user'];?>" placeholder="Username de acesso ao Switch" /> <br/>
										
										<b>Password</b><small>-Password de acesso ao Switch</small><input name="pass" type="text"
										class="form-control"  value="<?php echo $row['pass'];?>" placeholder="Password de acesso ao Switch" /><br/>
										    
					
                                              
                                         <b>Estado: </b> <small>backups ativos ou desativos?</small><br>
                                         <b>Backups ativos </b> <input type="checkbox" name = "estado" value="ativo"/><br/>
        
                                    </div> 
                                
                                    
                                    <input type="hidden" name="id" value="<?php echo $switchid;?>" />
                                    <input type="hidden" name="model" value="<?php echo $row['model'];?>" />
                                    <input type="hidden" name="brand" value="<?php echo $row['brand'];?>" />
                                    <h4><span id="result"></span></h4><br><br>
                                    
                                    <button id="editButton" class="btn btn-primary">Save changes</button>  
                                     
                                    </form>         
                                    
                                                              
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->
                        
                        
                    </div>
                    
                    
                    <div class="row">
                       <div class="col-md-12">
                            <div class="box box-info">
                                <div class="box-header">
                                <br>
                                <i class="fa fa-terminal"></i>
                                    <h3 class="box-title">Comandos</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body"> 
                                
                                <form id = "newCommands" method="post" action="editCommands.php">
                                   <div class="form-group">
									<h4><label>Painel de edição dos comandos do terminal</label></h4>
									<br>
									<div class="alert alert-info alert-dismissable">
                                        <i class="fa fa-info"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Atenção!</b> Todas as alterações efectuadas neste menu serão reflectidas nos comandos do switch.
                                        Pode apagar, editar e acrescentar linhas como desejar.<br>
                                        <b>Cada linha é intrepretada como um comando</b>
                                    </div>
                                    <span id="result1"></span>
									 <textarea name="editCommands" id="editCommands" class="form-controlConf2" rows="3"><?php echo $commands;?></textarea>
									</div>
									<input type="hidden" name="id" value="<?php echo $switchid;?>" />
				
				
									 <button id="editCommandsButton" class="btn btn-primary">Guardar alterações</button>
                                </form>
                                	
                                    
                                    
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->
                        
                        </div>
				

		
			</section>
			<!-- /.content -->
		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->


	<!-- jQuery 2.0.2 -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>

	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js"
		type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js"
		type="text/javascript"></script>
	<script src="js/testeScript.js" type="text/javascript"></script>
	
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
	
	
<!-- 	<script> -->
<!-- 	 	$(document).ready(function(){  -->
<!-- 	 	  $("#configs").click(function(){  -->
<!-- 	 	    $("#div1").fadeIn();  -->
<!-- 	 	  });  -->
<!-- 	 	});  -->

<!--  	</script>  -->
	
	<script>
		$("#editButton").click( function(){
			
			var data = $("#editForm :input").serializeArray();
			$.post( $("#editForm").attr("action"), data,function(info){ $("#result").html(info)} );
			
		});
		
		
		$("#editForm").submit(function(){
			return false;
		});

	</script>
	
	
	
	<script>
		$("#editCommandsButton").click( function(){
			
			var data = $("#newCommands :input").serializeArray();
			$.post( $("#newCommands").attr("action"), data,function(info){ $("#result1").html(info)} );
			
		});
		
		
		$("#newCommands").submit(function(){
			return false;
		});

	</script>
	
	
	

</body>
</html>