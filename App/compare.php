<?php
/*
  class.Diff.php

  A class containing a diff implementation

  Created by Stephen Morley - http://stephenmorley.org/ - and released under the
  terms of the CC0 1.0 Universal legal code:

  http://creativecommons.org/publicdomain/zero/1.0/legalcode
 */

// A class containing functions for computing diffs and formatting the output.
class Diff {

    // define the constants
    const UNMODIFIED = 0;
    const DELETED = 1;
    const INSERTED = 2;

    /* Returns the diff for two strings. The return value is an array, each of
     * whose values is an array containing two values: a line (or character, if
     * $compareCharacters is true), and one of the constants DIFF::UNMODIFIED (the
     * line or character is in both strings), DIFF::DELETED (the line or character
     * is only in the first string), and DIFF::INSERTED (the line or character is
     * only in the second string). The parameters are:
     *
     * $string1           - the first string
     * $string2           - the second string
     * $compareCharacters - true to compare characters, and false to compare
     *                      lines; this optional parameter defaults to false
     */

    public static function compare(
    $string1, $string2, $compareCharacters = false) {

        // initialise the sequences and comparison start and end positions
        $start = 0;
        if ($compareCharacters) {
            $sequence1 = $string1;
            $sequence2 = $string2;
            $end1 = strlen($string1) - 1;
            $end2 = strlen($string2) - 1;
        } else {
            $sequence1 = preg_split('/\R/', $string1);
            $sequence2 = preg_split('/\R/', $string2);
            $end1 = count($sequence1) - 1;
            $end2 = count($sequence2) - 1;
        }

        // skip any common prefix
        while ($start <= $end1 && $start <= $end2 && $sequence1[$start] == $sequence2[$start]) {
            $start ++;
        }

        // skip any common suffix
        while ($end1 >= $start && $end2 >= $start && $sequence1[$end1] == $sequence2[$end2]) {
            $end1 --;
            $end2 --;
        }

        // compute the table of longest common subsequence lengths
        $table = self::computeTable($sequence1, $sequence2, $start, $end1, $end2);

        // generate the partial diff
        $partialDiff = self::generatePartialDiff($table, $sequence1, $sequence2, $start);

        // generate the full diff
        $diff = array();
        for ($index = 0; $index < $start; $index ++) {
            $diff[] = array($sequence1[$index], self::UNMODIFIED);
        }
        while (count($partialDiff) > 0)
            $diff[] = array_pop($partialDiff);
        for ($index = $end1 + 1; $index < ($compareCharacters ? strlen($sequence1) : count($sequence1)); $index ++) {
            $diff[] = array($sequence1[$index], self::UNMODIFIED);
        }

        // return the diff
        return $diff;
    }

    /* Returns the table of longest common subsequence lengths for the specified
     * sequences. The parameters are:
     *
     * $sequence1 - the first sequence
     * $sequence2 - the second sequence
     * $start     - the starting index
     * $end1      - the ending index for the first sequence
     * $end2      - the ending index for the second sequence
     */

    private static function computeTable(
    $sequence1, $sequence2, $start, $end1, $end2) {

        // determine the lengths to be compared
        $length1 = $end1 - $start + 1;
        $length2 = $end2 - $start + 1;

        // initialise the table
        $table = array(array_fill(0, $length2 + 1, 0));

        // loop over the rows
        for ($index1 = 1; $index1 <= $length1; $index1 ++) {

            // create the new row
            $table[$index1] = array(0);

            // loop over the columns
            for ($index2 = 1; $index2 <= $length2; $index2 ++) {

                // store the longest common subsequence length
                if ($sequence1[$index1 + $start - 1] == $sequence2[$index2 + $start - 1]) {
                    $table[$index1][$index2] = $table[$index1 - 1][$index2 - 1] + 1;
                } else {
                    $table[$index1][$index2] = max($table[$index1 - 1][$index2], $table[$index1][$index2 - 1]);
                }
            }
        }

        // return the table
        return $table;
    }

    /* Returns the partial diff for the specificed sequences, in reverse order.
     * The parameters are:
     *
     * $table     - the table returned by the computeTable function
     * $sequence1 - the first sequence
     * $sequence2 - the second sequence
     * $start     - the starting index
     */

    private static function generatePartialDiff(
    $table, $sequence1, $sequence2, $start) {

        //  initialise the diff
        $diff = array();

        // initialise the indices
        $index1 = count($table) - 1;
        $index2 = count($table[0]) - 1;

        // loop until there are no items remaining in either sequence
        while ($index1 > 0 || $index2 > 0) {

            // check what has happened to the items at these indices
            if ($index1 > 0 && $index2 > 0 && $sequence1[$index1 + $start - 1] == $sequence2[$index2 + $start - 1]) {

                // update the diff and the indices
                $diff[] = array($sequence1[$index1 + $start - 1], self::UNMODIFIED);
                $index1 --;
                $index2 --;
            } elseif ($index2 > 0 && $table[$index1][$index2] == $table[$index1][$index2 - 1]) {

                // update the diff and the indices
                $diff[] = array($sequence2[$index2 + $start - 1], self::INSERTED);
                $index2 --;
            } else {

                // update the diff and the indices
                $diff[] = array($sequence1[$index1 + $start - 1], self::DELETED);
                $index1 --;
            }
        }

        // return the diff
        return $diff;
    }

    /* Returns a diff as an HTML table. The parameters are:
     *
     * $diff        - the diff array
     * $indentation - indentation to add to every line of the generated HTML; this
     *                optional parameter defaults to ''
     * $separator   - the separator between lines; this optional parameter
     *                defaults to '<br>'
     */

    public static function toTable($diff, $h1, $h2, $indentation = '', $separator = '') {

        // initialise the HTML
        $html = $indentation . "<div style=\"overflow-x:scroll; \">\n<table class=\"diff\" style=\"width:100%; table-layout:auto;\">\n";

        $html .= "<th style=\"border:1px solid rgb(100,100,100); background:rgb(220,220,220);\">".$h1."</th>"."<th>"."</th>"."<th style=\"border:1px solid rgb(100,100,100); background:rgb(220,220,220);\">".$h2."</th>";

        // loop over the lines in the diff
        $index = 0;
        while ($index < count($diff)) {

            // determine the line type
            switch ($diff[$index][1]) {

                // display the content on the left and right
                case self::UNMODIFIED:
                    $leftCell = self::getCellContent(
                                    $diff, $indentation, $separator, $index, self::UNMODIFIED);
                    $rightCell = $leftCell;
                    break;

                // display the deleted on the left and inserted content on the right
                case self::DELETED:
                    $leftCell = self::getCellContent(
                                    $diff, $indentation, $separator, $index, self::DELETED);
                    $rightCell = self::getCellContent(
                                    $diff, $indentation, $separator, $index, self::INSERTED);
                    break;

                // display the inserted content on the right
                case self::INSERTED:
                    $leftCell = '';
                    $rightCell = self::getCellContent(
                                    $diff, $indentation, $separator, $index, self::INSERTED);
                    break;
            }

            // extend the HTML with the new row
            $html .=
                    $indentation
                    . "  <tr>\n"
                    . $indentation
                    . '    <td class="diff'
                    . ($leftCell == $rightCell ? 'Unmodified' : ($leftCell == '' ? 'Blank' : 'Deleted'))
                    . '">'
                    . $leftCell
                    . "</td>\n<td/>"
                    . $indentation
                    . '    <td class="diff'
                    . ($leftCell == $rightCell ? 'Unmodified' : ($rightCell == '' ? 'Blank' : 'Inserted'))
                    . '">'
                    . $rightCell
                    . "</td>\n"
                    . $indentation
                    . "  </tr>\n";
        }
        //$html = str_replace(" ","&nbsp;",$html);
        // return the HTML
        return $html . $indentation . "</table>\n</div>\n";
    }

    /* Returns the content of the cell, for use in the toTable function. The
     * parameters are:
     *
     * $diff        - the diff array
     * $indentation - indentation to add to every line of the generated HTML
     * $separator   - the separator between lines
     * $index       - the current index, passes by reference
     * $type        - the type of line
     */

    private static function getCellContent(
    $diff, $indentation, $separator, &$index, $type) {

        // initialise the HTML
        $html = '';

        // loop over the matching lines, adding them to the HTML
        while ($index < count($diff) && $diff[$index][1] == $type) {
            $html .=
                    '<span>'
                    . str_replace(" ", "&nbsp;", htmlspecialchars($diff[$index][0]))
                    . '</span>'
                    . $separator;
            $index ++;
        }

        // return the HTML
        return $html;
    }

}

session_start();

try {

	$servername = "localhost";
	$username = "root";
	$password = "nhe";
	$dbname = "ES";

    $bd = mysqli_connect($servername, $username, $password, $dbname);
    if ($bd->connect_error) {
        $bd = false;
    }
} catch (Exception $e) {
    $bd = false;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>HelpDESK | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
        .diff th{
                font-size:1.1em;
                line-height:1.333;
                padding:0 10px;
            }

            .diff td{
                white-space: nowrap;
                padding:0 0.667em;
                vertical-align:top;
                font-family:Consolas,'Courier New',Courier,monospace;
                font-size:1em;
                line-height:1.333;
            }

            .diff span{
                display: block;
                width: 100%;
                min-height:1.333em;
                margin-top:-1px;
                padding:0 3px;
            }

            * html .diff span{
                height:1.333em;
            }

            .diff span:first-child{
                margin-top:1px;
            }

            .diffDeleted span{
                border:1px solid rgb(255,192,192);
                background:rgb(255,224,224);
            }

            .diffInserted span{
                border:1px solid rgb(192,255,192);
                background:rgb(224,255,224);
            }

            #toStringOutput{
                margin:0 2em 2em;
            }

        </style>
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                HelpDESK
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->

                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->

                        <!-- Notifications: style can be found in dropdown.less -->

                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Utilizador <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="img/avatar5.png" class="img-circle" alt="User Image" />
                                    <p>
                                        Set Team - Web Developer

                                    </p>
                                </li>
                                <!-- Menu Body -->

                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="perfil.php" class="btn btn-default btn-flat">Perfil</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="#" class="btn btn-default btn-flat">Sair</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="img/avatar5.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Bem-Vindo</p>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
					<li><a href="adicionar.php"> <i class="fa fa-plus"></i> <span>Adicionar</span>
					</a>
					</li>
					<li>
						<a href="home.php" onclick="submitHistorico();">
						<i class="fa fa-arrow-left"></i>
						<span>Voltar</span> </a>
					</li>
				</ul>
			</section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Switches
                    </h1>


                </section>

                <!-- Main content -->


                <section class="content">
                    <?php
                    if ($bd !== false && ( isset($_POST["radio"]) || isset($_SESSION["switchid"]) )) {
                        if (isset($_POST["radio"])) {
                            $_SESSION["switchid"] = $_POST["radio"];
                        }

                        $switchid = $_SESSION["switchid"];
                        ?>

                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">
                                    <?php
                                    $sql = "SELECT name, ip FROM switches WHERE id = " . $switchid;
                                    $result = $bd->query($sql);

                                    while ($row = $result->fetch_assoc()) {
                                        echo "SWITCH:&nbsp;&nbsp;&nbsp;" . $row['name'] . "&nbsp;&nbsp;-&nbsp;&nbsp;" . $row['ip'];
                                    }
                                    ?>
                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <?php
                                if (isset($_POST["compare"])) {
                                    $ids = $_POST["compare"];
                                    if (count ($ids) != 2)
                                    {
                                        echo "<h4 style=\"color: rgb(200,80,80);\">Selecione 2 e só 2 backups a comparar!</h4><br>";
                                    }
                                    else
                                    {
                                        $sql = "SELECT config, switchid, date FROM backups WHERE id = " . $ids[0];
                                        $result = $bd->query($sql);

                                        if ($result->num_rows > 0) {
                                            // output data of each row
                                            while ($row = $result->fetch_assoc()) {
                                                $s1 = $row["config"];
                                                $head1 = "Backup de ".$row["date"];
                                            }
                                        } else {
                                            echo "0 results";
                                        }

                                        $sql = "SELECT config, switchid, date FROM backups WHERE id = " . $ids[1];
                                        $result = $bd->query($sql);

                                        if ($result->num_rows > 0) {
                                            // output data of each row
                                            while ($row = $result->fetch_assoc()) {
                                                $s2 = $row["config"];
                                                $head2 = "Backup de ".$row["date"];
                                            }
                                        } else {
                                            echo "0 results";
                                        }
                                        $bd->close();

                                        $diff = Diff::compare($s1, $s2);
                                        echo Diff::toTable($diff, $head1, $head2);
                                    }
                                }

                            } else {
                                echo "<h4 style=\"color: rgb(200,80,80);\">Problemas ao abrir esta página!</h4><br>";
                            }
                            ?>

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(function () {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>

    </body>
</html>
