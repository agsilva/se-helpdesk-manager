<?php

session_start();

try {

	$servername = "localhost";
	$username = "root";
	$password = "nhe";
	$dbname = "ES";

	$bd = mysqli_connect($servername, $username, $password, $dbname);
	if ($bd->connect_error) {
		$bd = false;
	}

} catch (Exception $e) {
	$bd = false;
}

?>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HelpDESK | Histórico</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
	<!-- header logo: style can be found in header.less -->
	<header class="header">
		<a href="home.php" class="logo"> <!-- Add the class icon to your logo image or logo icon to add the margining -->
			HelpDESK
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
			<!-- Sidebar toggle button-->

		</nav>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="img/avatar5.png" class="img-circle" alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>Bem-Vindo</p>
					</div>
				</div>

				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li><a href="adicionar.php"> <i class="fa fa-plus"></i> <span>Adicionar</span>
					</a>
					</li>
					<li><a href="home.php" onclick="submitHistorico();"> <i
							class="fa fa-arrow-left"></i> <span>Voltar</span>
					</a>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Histórico</h1>


			</section>


			<!-- Link com uma cena para contar o numero de checkboxes

                http://stackoverflow.com/questions/18572351/counting-how-many-checkbox-are-checked-php-html
                -->
			<!-- Main content -->
			<section class="content">
				<?php
				if ($bd !== false && ( isset($_POST["radio"]) || isset($_SESSION["switchid"]) )) {
					if (isset($_POST["radio"])) {
						$_SESSION["switchid"] = $_POST["radio"];
					}

					$switchid = $_SESSION["switchid"];
					?>

				<div class="box">
					<div class="box-header">
						<h3 class="box-title">
							<?php
							$sql = "SELECT name, ip FROM switches WHERE id = " . $switchid;
							$result = $bd->query($sql);

							while ($row = $result->fetch_assoc()) {
								echo "SWITCH:&nbsp;&nbsp;&nbsp;".$row['name'] . "&nbsp;&nbsp;-&nbsp;&nbsp;" . $row['ip'];
							}
							?>
						</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive">
						<form name="myform" method="post" action="#">
							<table id="example1" class="table table-bordered table-striped">
								<col>
								<col width="100">
								<thead>
									<tr>
										<th>Data e hora</th>
										<th>Selecção</th>
									</tr>
								</thead>
								<tbody>

									<?php


									$sql = "SELECT date, id FROM backups WHERE switchid = " . $switchid;
									$result = $bd->query($sql);

									while ($row = $result->fetch_assoc()) {
										?>

									<tr>
										<td><?php echo $row['date']; ?></td>
										<td><div class="checkbox">

												<label> <input type="checkbox"
													value=<?php echo "\"".$row['id']."\""; ?> name="compare[]" />
												</label>
											</div>
										</td>
									</tr>

									<?php
									}
				} else {
					echo "<h4 style=\"color: rgb(200,80,80);\">Problemas ao abrir esta página!</h4><br>";
				}
				?>



								</tbody>
								<tfoot>
									<tr>
										<th>Data e hora</th>
										<th>Selecção</th>
									</tr>
								</tfoot>

							</table>
							<button class="btn btn-primary" onclick="submitCompare();">Comparar</button>
						</form>


					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


			</section>
			<!-- /.content -->
		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->


	<!-- jQuery 2.0.2 -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>

	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js"
		type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js"
		type="text/javascript"></script>

	<!-- page script -->
	<script type="text/javascript">
            $(function () {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true
                });
            });
        </script>

	<script type="text/javascript">

            function submitCompare()
            {
              document.myform.action="class.Diff.php";
              document.myform.submit();
            }

        </script>

</body>
</html>
