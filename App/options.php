<?php 

try {

	$servername = "localhost";
	$username = "root";
	$password = "nhe";
	$dbname = "ES";

	$bd = mysqli_connect($servername, $username, $password, $dbname);
	if ($bd->connect_error) {
		$bd = false;
	}

} catch (Exception $e) {
	$bd = false;
}



if($bd!= false){
	$sql = "SELECT time FROM configs WHERE name ='interval'";	
	$result = $bd->query($sql) or die();
	
	$row = $result->fetch_assoc();
	$interval = $row['time'];
	
	$sql = "SELECT time FROM configs WHERE name ='duration'";
	$result = $bd->query($sql) or die();
	
	$row = $result->fetch_assoc();
	$duration = $row['time'];
	
	
}
else{
	
	echo "Problema ao ligar à base de dados";
	
}

	

?>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HelpDESK | Adicionar</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- daterange picker -->
<link href="css/daterangepicker/daterangepicker-bs3.css"
	rel="stylesheet" type="text/css" />
<!-- iCheck for checkboxes and radio inputs -->
<link href="css/iCheck/all.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Color Picker -->
<link href="css/colorpicker/bootstrap-colorpicker.min.css"
	rel="stylesheet" />
<!-- Bootstrap time Picker -->
<link href="css/timepicker/bootstrap-timepicker.min.css"
	rel="stylesheet" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
	<!-- header logo: style can be found in header.less -->
	<header class="header">
		<a href="home.php" class="logo"> <!-- Add the class icon to your logo image or logo icon to add the margining -->
			HelpDESK
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
			<!-- Sidebar toggle button-->

		</nav>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="img/avatar5.png" class="img-circle" alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>Bem-Vindo</p>
					</div>
				</div>

				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li><a href="adicionar.php"> <i class="fa fa-plus"></i> <span>Adicionar</span>
					</a>
					</li>
					<li><a href="home.php" onclick="submitHistorico();"> <i
							class="fa fa-arrow-left"></i> <span>Voltar</span>
					</a>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Opções</h1>


			</section>

			<!-- Main content -->
			<section class="content">
				<form id="myForm" name="myForm" method="post"
					action="addOptions.php">
					<div class="box-body">
						<div class="form-group">
							<label for="intervalo">Intervalo de Backup (Segundos)</label> <input
								class="form-control2" name="backupInterval" <?php echo "value='". $interval."'"?>>
						</div>
						<div class="form-group">
							<label for="duracao">Duração de Backup (Segundos)</label> <input
								class="form-control2" name="backupDuration" <?php echo "value='". $duration."'"?> >
						</div>

						<div class="box-footer">
							<button id="editCommandsButton" type="submit" class="btn btn-primary">Gravar</button>
						</div>
						
						<span id="result1"></span>

					</div>
					<!-- /.box-body -->
				</form>
			</section>
			<!-- /.content -->
		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->


	<!-- jQuery 2.0.2 -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	
	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>

	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js"
		type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js"
		type="text/javascript"></script>


		
		<script>
		$("#editCommandsButton").click( function(){
			
			var data = $("#myForm :input").serializeArray();
			$.post( $("#myForm").attr("action"), data,function(info){ $("#result1").html(info)} );
			
		});
		
		
		$("#myForm").submit(function(){
			return false;
		});

	</script>
	<!-- page script -->
	<script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>

	<!-- Page script -->
	

</body>
</html>
