<?php

if(isset($_POST['model']) and isset($_POST['brand']) and isset($_POST['commands']) and isset($_POST["name"]) and isset($_POST["type"]) and isset($_POST["ip"]) and isset($_POST["user"]) and isset($_POST["pass"]) )
{
	
	$pass = $_POST["pass"];
	$name = $_POST["name"];
	$user = $_POST["user"];
	$ip = $_POST["ip"];
	$type = $_POST["type"]; 

	
	$model = $_POST["model"];
	$brand = $_POST["brand"];


	
	$bd = new PDO('mysql:host=localhost;dbname=ES', 'root', 'nhe');
	
	if($bd !== false)
	{
		
		$stmt = $bd->prepare("INSERT INTO switches (ip, name, user, pass,type,model,brand) VALUES (:ip, :name, :user, :pass, :type, :model, :brand)");
		$stmt->bindParam(':ip', $ip);
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':user', $user);
		$stmt->bindParam(':pass', $pass);
		$stmt->bindParam(':type', $type);
		$stmt->bindParam(':model', $model);
		$stmt->bindParam(':brand', $brand);
		$stmt->execute();

		
		
		$commands = preg_split("/\r\n|\n|\r/",$_POST['commands']);
		
		
		$switchid = $bd->lastInsertId();

 		
		$stmtc = $bd->prepare("INSERT INTO commands (command, switchid) VALUES (:command, :switchid)");
		$i = 0;
		
		while ($i< count($commands) - 1)
		{
			$stmtc->bindParam(':switchid', $switchid);
			$stmtc->bindParam(':command', $commands[$i]);
			$stmtc->execute();
			$i++;
		}
		
		echo "<div class='alert alert-success alert-dismissable'>
		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
		<b>Já está!</b> Novo Switch adicionado com sucesso!
		</div>";

	}else{
		echo "<div class='alert alert-danger alert-dismissable'>
				<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
				<b>Ocorreu um problema</b> Não foi possivel adicionar o Switch
				</div>";
	}
	
	
}else{
			echo "<div class='alert alert-danger alert-dismissable'>
				<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
				<b>Ocorreu um problema</b> Não foi possivel adicionar o Switch: problema ao ligar à base de dados
				</div>";
	}


?>