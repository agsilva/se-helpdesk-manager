<?php
//include_once('ligacaoBaseDados.php');

session_start();

try {

	$servername = "localhost";
	$username = "root";
	$password = "nhe";
	$dbname = "ES";

	$bd = mysqli_connect($servername, $username, $password, $dbname);
	if ($bd->connect_error) {
		$bd = false;
	}

} catch (Exception $e) {
	$bd = false;
}



if($bd !== false)
{
	$sql = "SELECT id,name,brand,model,ip,estado FROM switches ORDER BY id";
	$result = $bd->query($sql);


}
else{
	echo "NAO LIGOU À BD";
}


?>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HelpDESK | Dashboard</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
	<!-- header logo: style can be found in header.less -->
	<header class="header">
		<a href="home.php" class="logo"> <!-- Add the class icon to your logo image or logo icon to add the margining -->
			HelpDESK
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
			<!-- Sidebar toggle button-->

			
		</nav>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="img/avatar5.png" class="img-circle" alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>Bem-Vindo</p>
					</div>
				</div>

				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li><a href="adicionar.php"> <i class="fa fa-plus"></i> <span>Adicionar</span>
					</a>
					</li>
					<li><a href="#" onclick="submitConfigs();"><i class="fa fa-edit"></i> <span>Configurações</span>
					</a>
					</li>
					</li>

					<li>
						<!-- <a href="#" onclick="post('/teste/historico.php',{switchSelected: '1'});">-->
						<a href="#" onclick="submitHistorico();">
						<i class="fa fa-archive"></i>
						<span>Histórico</span> </a>
					</li>
					
					<li>
						<a href="options.php">
						<i class="fa fa-gear"></i>
						<span>Opções</span> </a>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Switches</h1>


			</section>

			<!-- Main content -->


			<section class="content">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Titulo</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive">
						<form name="myform" method="post">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Nome</th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Local</th>
										<th>Status</th>
										<th>Selecção</th>
									</tr>
								</thead>
								<tbody>

									<?php 
									while ($row = $result->fetch_assoc()) {

										echo "<tr class = 'even'>\n";
										echo "<td>".$row['name']."</td>";
										echo "<td>".$row['brand']."</td>";
										echo "<td>".$row['model']."</td>";
										echo "<td>".$row['ip']."</td>";
										if ($row['estado'] == 1)
											echo "<td>Ativo</td>";
										else
											echo "<td>Inativo</td>";
										echo "
										<td>
											<div class='radio'>
												<label>
													<input type='radio' name='radio' value='".$row['id']."' checked></input>
												</label>
											</div>
										</td>
									</tr>";


 								}?>

								</tbody>
								<tfoot>
									<tr>
										<th>Nome</th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Local</th>
										<th>Status</th>
										<th>Selecção</th>
									</tr>
								</tfoot>
							</table>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
	

	</section>
	<!-- /.content -->
	</aside>
	<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->


	<!-- jQuery 2.0.2 -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>

	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js"
		type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js"
		type="text/javascript"></script>

	<!-- page script -->

<!-- 	<script type="text/javascript"> -->
        
<!-- 	function post(path, params, method) { // Faz post para um dado URL  -->
<!-- 	    method = method || "post";  -->

<!-- 	    var form = document.createElement("form"); -->
<!-- 	    form.setAttribute("method", method); -->
<!-- 	    form.setAttribute("action", path); -->

<!-- 	    for(var key in params) { -->
<!-- 	        if(params.hasOwnProperty(key)) { -->
<!-- 	            var hiddenField = document.createElement("input"); -->
<!-- 	            hiddenField.setAttribute("type", "hidden"); -->
<!-- 	            hiddenField.setAttribute("name", key); -->
<!-- 	            hiddenField.setAttribute("value", params[key]); -->

<!-- 	            form.appendChild(hiddenField); -->
<!-- 	         } -->
<!-- 	    } -->

<!-- 	    document.body.appendChild(form); -->
<!-- 	    form.submit(); -->
<!-- 	} -->
<!--     </script> -->
    
    
    
    <script type="text/javascript">

    function submitHistorico()
    {
      document.myform.action="historico.php";
      document.myform.submit();
    }
    
    </script>
    
    
    <script type="text/javascript">

    function submitConfigs()
    {
      document.myform.action="configuracoes.php"; 
      document.myform.submit();
    }
    
    </script>
    
    
    
        
        <script type="text/javascript">
            $(function() {
               
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": true
                });

                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": false,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>

    </body>
</html>
